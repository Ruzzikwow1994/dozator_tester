/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MatrixLed_Y0_Pin GPIO_PIN_0
#define MatrixLed_Y0_GPIO_Port GPIOC
#define MatrixLed_Y1_Pin GPIO_PIN_1
#define MatrixLed_Y1_GPIO_Port GPIOC
#define MatrixLed_Y2_Pin GPIO_PIN_2
#define MatrixLed_Y2_GPIO_Port GPIOC
#define MatrixLed_Y3_Pin GPIO_PIN_3
#define MatrixLed_Y3_GPIO_Port GPIOC
#define MatrixLed_Y4_Pin GPIO_PIN_4
#define MatrixLed_Y4_GPIO_Port GPIOC
#define MatrixLed_Y5_Pin GPIO_PIN_5
#define MatrixLed_Y5_GPIO_Port GPIOC
#define RES_MatrixLed_Pin GPIO_PIN_1
#define RES_MatrixLed_GPIO_Port GPIOB
#define RES_Green_Pin GPIO_PIN_2
#define RES_Green_GPIO_Port GPIOB
#define Motor_SW_2_Pin GPIO_PIN_10
#define Motor_SW_2_GPIO_Port GPIOB
#define Motor_SW_2_EXTI_IRQn EXTI15_10_IRQn
#define Motor_SW_4_Pin GPIO_PIN_12
#define Motor_SW_4_GPIO_Port GPIOB
#define Motor_SW_4_EXTI_IRQn EXTI15_10_IRQn
#define Motor_SW_5_Pin GPIO_PIN_13
#define Motor_SW_5_GPIO_Port GPIOB
#define Motor_SW_5_EXTI_IRQn EXTI15_10_IRQn
#define Motor_SW_6_Pin GPIO_PIN_14
#define Motor_SW_6_GPIO_Port GPIOB
#define Motor_SW_6_EXTI_IRQn EXTI15_10_IRQn
#define Motor_SW_7_Pin GPIO_PIN_15
#define Motor_SW_7_GPIO_Port GPIOB
#define Motor_SW_7_EXTI_IRQn EXTI15_10_IRQn
#define MatrixLed_Y6_Pin GPIO_PIN_6
#define MatrixLed_Y6_GPIO_Port GPIOC
#define MatrixLed_Y7_Pin GPIO_PIN_7
#define MatrixLed_Y7_GPIO_Port GPIOC
#define RES_Motor_Pin GPIO_PIN_8
#define RES_Motor_GPIO_Port GPIOC
#define RES_SWMatrix_Pin GPIO_PIN_4
#define RES_SWMatrix_GPIO_Port GPIOB
#define RES_FromSW_Pin GPIO_PIN_5
#define RES_FromSW_GPIO_Port GPIOB
#define Motor_SW_0_Pin GPIO_PIN_8
#define Motor_SW_0_GPIO_Port GPIOB
#define Motor_SW_0_EXTI_IRQn EXTI9_5_IRQn
#define Motor_SW_1_Pin GPIO_PIN_9
#define Motor_SW_1_GPIO_Port GPIOB
#define Motor_SW_1_EXTI_IRQn EXTI9_5_IRQn
/* USER CODE BEGIN Private defines */
#pragma anon_unions;

#define STROBE_MatrixLed				HAL_GPIO_WritePin(Lach_MatrixLed_GPIO_Port, Lach_MatrixLed_Pin, GPIO_PIN_RESET);  HAL_GPIO_WritePin(Lach_MatrixLed_GPIO_Port, Lach_MatrixLed_Pin, GPIO_PIN_SET)
#define STROBE_MatrixSW					HAL_GPIO_WritePin(Lach_MatrixSW_GPIO_Port, 	Lach_MatrixSW_Pin, GPIO_PIN_RESET); 	HAL_GPIO_WritePin(Lach_MatrixSW_GPIO_Port,	Lach_MatrixSW_Pin, GPIO_PIN_SET)
#define STROBE_Motor						HAL_GPIO_WritePin(Lach_Motor_GPIO_Port, 		Lach_Motor_Pin, GPIO_PIN_RESET);  		HAL_GPIO_WritePin(Lach_Motor_GPIO_Port, 		Lach_Motor_Pin, GPIO_PIN_SET)
#define RESET_SHIFTREG				HAL_GPIO_WritePin(GPIOB, RES_MatrixLed_Pin|RES_Green_Pin|RES_SWMatrix_Pin|RES_FromSW_Pin, GPIO_PIN_RESET); HAL_GPIO_WritePin(GPIOB, RES_MatrixLed_Pin|RES_Green_Pin|RES_SWMatrix_Pin|RES_FromSW_Pin, GPIO_PIN_SET)
union frame {
	uint8_t LED_MATRIX;
	uint8_t GREEN_LED;
	uint8_t RED_LED;
	uint8_t SW_MATRIX;
	uint8_t MOTOR;
	struct  {
	unsigned Led_Matrix_0:1;
	unsigned Led_Matrix_1:1;
	unsigned Led_Matrix_2:1;
	unsigned Led_Matrix_3:1;
	unsigned Led_Matrix_4:1;
	unsigned Led_Matrix_5:1;
	unsigned Led_Matrix_6:1;
	unsigned Led_Matrix_7:1;
	
	unsigned Green_Led_0:1;
	unsigned Green_Led_1:1;
	unsigned Green_Led_2:1;
	unsigned Green_Led_3:1;
	unsigned Green_Led_4:1;
	unsigned Green_Led_5:1;
	unsigned Green_Led_6:1;
	unsigned Green_Led_7:1;
	
	unsigned Red_Led_0:1;
	unsigned Red_Led_1:1;
	unsigned Red_Led_2:1;
	unsigned Red_Led_3:1;
	unsigned Red_Led_4:1;
	unsigned Red_Led_5:1;
	unsigned Red_Led_6:1;
	unsigned Red_Led_7:1;
	
	unsigned Matrix_SW_0:1;
	unsigned Matrix_SW_1:1;
	unsigned Matrix_SW_2:1;
	unsigned Matrix_SW_3:1;
	unsigned Matrix_SW_4:1;
	unsigned Matrix_SW_5:1;
	unsigned Matrix_SW_6:1;
	unsigned Matrix_SW_7:1;
	
	unsigned Motor_0:1;
	unsigned Motor_1:1;
	unsigned Motor_2:1;
	unsigned Motor_3:1;
	unsigned Motor_4:1;
	unsigned Motor_5:1;
	unsigned Motor_6:1;
	unsigned Motor_7:1;
	}	spi_frame;
	};
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
